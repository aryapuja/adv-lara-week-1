<?php

/* Trait */
trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    /* Setter - Getter */
        public function set_nama($nama)
        {
            $this->nama = $nama;
        }

        public function get_nama()
        {
            return $this->nama;
        }

        public function set_darah($darah)
        {
            $this->darah = $darah;
        }

        public function get_darah()
        {
            return $this->darah;
        }

        public function set_jumlahKaki($jumlahKaki)
        {
            $this->jumlahKaki = $jumlahKaki;
        }

        public function get_jumlahKaki()
        {
            return $this->jumlahKaki;
        }

        public function set_keahlian($keahlian)
        {
            $this->keahlian = $keahlian;
        }

        public function get_keahlian()
        {
            return $this->keahlian;
        }
    /* Setter - Getter */

    public function atraksi()
    {
        return $this->nama.' sedang '.$this->keahlian;
    }

}

trait Fight{
    use Hewan;
    public $attackPower;
    public $defensePower;

    /* Setter - Getter */
        public function set_attackPower($attackPower)
        {
            $this->attackPower = $attackPower;
        }

        public function get_attackPower()
        {
            return $this->attackPower;
        }

        public function set_defensePower($defensePower)
        {
            $this->defensePower = $defensePower;
        }

        public function get_defensePower()
        {
            return $this->defensePower;
        }
    /* Setter - Getter */
    
    public function serang($nama)
    {
        return $this->nama." sedang menyerang ".$nama;
    }

    public function diserang($attackPower)
    {
        $this->set_darah($this->get_darah()-($attackPower/$this->get_defensePower())); //update nilai darah
        echo $this->nama." sedang diserang";
        echo '<br>';
        echo 'Sisa Nyawa dari '.$this->get_nama().' adalah '.$this->get_darah();
    }
}
/* Trait */

/* Class */
class Elang{
    use Fight, Hewan;
    public function getInfoHewan()
    {
        return 'Nama: '.$this->get_nama().' || Jumlah Kaki: '.$this->get_jumlahKaki().' || Keahlian: '.$this->get_keahlian().' || Attack/Defense = '.$this->get_attackPower().'/'.$this->get_defensePower().' || Sisa Nyawa: '.$this->get_darah();
    }
}

class Harimau{
    use Fight, Hewan;
    public function getInfoHewan()
    {
        return 'Nama: '.$this->get_nama().' || Jumlah Kaki: '.$this->get_jumlahKaki().' || Keahlian: '.$this->get_keahlian().' || Attack/Defense = '.$this->get_attackPower().'/'.$this->get_defensePower().' || Sisa Nyawa: '.$this->get_darah();
    }
}
/* Class */


$elang_1 = new Elang();
$elang_1->set_nama('elang_1');
$elang_1->set_jumlahKaki(2);
$elang_1->set_keahlian("Terbang Tinggi");
$elang_1->set_attackPower(10);
$elang_1->set_defensePower(5);
echo'Nama: '.$elang_1->get_nama().' || Jumlah Kaki: '.$elang_1->get_jumlahKaki().' || Keahlian: '.$elang_1->get_keahlian().' || Attack/Defense = '.$elang_1->get_attackPower().'/'.$elang_1->get_defensePower();
echo '<br>';
echo $elang_1->atraksi();
echo '<br>';

echo '<br>';
$harimau_1 = new Harimau();
$harimau_1->set_nama('harimau_1');
$harimau_1->set_jumlahKaki(4);
$harimau_1->set_keahlian("Lari Cepat");
$harimau_1->set_attackPower(7);
$harimau_1->set_defensePower(8);
echo'Nama: '.$harimau_1->get_nama().' || Jumlah Kaki: '.$harimau_1->get_jumlahKaki().' || Keahlian: '.$harimau_1->get_keahlian().' || Attack/Defense = '.$harimau_1->get_attackPower().'/'.$harimau_1->get_defensePower();
echo '<br>';
echo $harimau_1->atraksi();
echo '<br>';

echo '<br>';
echo '- = Let\'s Start The Game = -';
echo '<br>==================================================<br>';
echo $harimau_1->serang($elang_1->get_nama());
echo '<br>';
echo $elang_1->diserang($harimau_1->get_attackPower());
echo '<br>==================================================<br>';
echo $harimau_1->serang($elang_1->get_nama());
echo '<br>';
echo $elang_1->diserang($harimau_1->get_attackPower());
echo '<br>==================================================<br>';
echo $elang_1->serang($harimau_1->get_nama());
echo '<br>';
echo $harimau_1->diserang($elang_1->get_attackPower());
echo '<br>==================================================<br>';
echo $elang_1->serang($harimau_1->get_nama());
echo '<br>';
echo $harimau_1->diserang($elang_1->get_attackPower());
echo '<br>==================================================<br>';

echo '<br>';
echo '- = F I N I S H = -';
echo '<br>';
echo $elang_1->getInfoHewan();
echo '<br>';
echo $harimau_1->getInfoHewan();

?>